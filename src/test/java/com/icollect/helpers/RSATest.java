package com.icollect.helpers;

import junit.framework.TestCase;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import static org.junit.Assert.*;

/**
 * Created by Benjamin Lu on 2015/12/4.
 */
public class RSATest extends TestCase
{
    RSAPrivateKey rsaPrivateKey;
    RSAPublicKey rsaPublicKey;
    public void setUp() throws Exception
    {
        super.setUp();
        KeyPair keyPair = RSA.generateKeyPair();
        rsaPrivateKey = (RSAPrivateKey) keyPair.getPrivate();
        rsaPublicKey = (RSAPublicKey) keyPair.getPublic();
    }

    public void tearDown() throws Exception
    {
        rsaPrivateKey = null;
        rsaPublicKey = null;
    }

    public void testGenerateKeyPair() throws Exception
    {
        assertNotNull(rsaPrivateKey);
        assertNotNull(rsaPublicKey);
        assertNotNull(rsaPrivateKey.getPrivateExponent());
        assertNotNull(rsaPrivateKey.getModulus());
        assertNotNull(rsaPublicKey.getPublicExponent());
        assertNotNull(rsaPublicKey.getModulus());
    }

    public void testGetPublicKey() throws Exception
    {
        byte[] exponent = rsaPublicKey.getPublicExponent().toByteArray();
        byte[] modulus = rsaPublicKey.getModulus().toByteArray();
        String base64Exponent = Base64.encodeToString(exponent, Base64.DEFAULT);
        String base64Modulus = Base64.encodeToString(modulus, Base64.DEFAULT);
        byte[] restoreExponent = Base64.decode(base64Exponent, Base64.DEFAULT);
        byte[] restoreModulus = Base64.decode(base64Modulus, Base64.DEFAULT);

        RSAPublicKey publicKey = RSA.getPublicKey(new BigInteger(restoreModulus), new BigInteger(restoreExponent));
        assertEquals(publicKey.getPublicExponent().toString(), rsaPublicKey.getPublicExponent().toString());
        assertEquals(publicKey.getModulus().toString(), rsaPublicKey.getModulus().toString());
    }

    public void testGetPrivateKey() throws Exception
    {
        byte[] exponent = rsaPrivateKey.getPrivateExponent().toByteArray();
        byte[] modulus = rsaPrivateKey.getModulus().toByteArray();
        String base64Exponent = Base64.encodeToString(exponent, Base64.DEFAULT);
        String base64Modulus = Base64.encodeToString(modulus, Base64.DEFAULT);
        byte[] restoreExponent = Base64.decode(base64Exponent, Base64.DEFAULT);
        byte[] restoreModulus = Base64.decode(base64Modulus, Base64.DEFAULT);

        RSAPrivateKey privateKey = RSA.getPrivateKey(new BigInteger(restoreModulus), new BigInteger(restoreExponent));
        assertEquals(privateKey.getPrivateExponent().toString(), rsaPrivateKey.getPrivateExponent().toString());
        assertEquals(privateKey.getModulus().toString(), rsaPrivateKey.getModulus().toString());
    }

    public void testEncrypt() throws Exception
    {
        String m = "raw message for test case";
        byte[] encrypted = RSA.encrypt(m, rsaPublicKey);
        String base64EncryptedString = Base64.encodeToString(encrypted, Base64.DEFAULT);
        assertNotEquals(base64EncryptedString, m);
    }

    public void testDecrypt() throws Exception
    {
        String m = "raw message for test case";
        byte[] encrypted = RSA.encrypt(m, rsaPublicKey);
        String base64EncryptedString = Base64.encodeToString(encrypted, Base64.DEFAULT);
        byte[] decodedEncrypted = Base64.decode(base64EncryptedString, Base64.DEFAULT);
        byte[] decrypted = RSA.decrypt(decodedEncrypted, rsaPrivateKey);
        String plaintext = new String(decrypted);
        assertEquals(m, plaintext);
    }

    public void testSign() throws Exception
    {
        String m = "sign message";
        byte[] sign = RSA.sign(m, rsaPrivateKey);
        String base64Sign = Base64.encodeToString(sign, Base64.DEFAULT);
        assertNotEquals(m, base64Sign);
    }

    public void testVerify() throws Exception
    {
        String m = "sign message";
        byte[] sign = RSA.sign(m, rsaPrivateKey);
        String base64Sign = Base64.encodeToString(sign, Base64.DEFAULT);
        byte[] receivedSign = Base64.decode(base64Sign, Base64.DEFAULT);
        boolean isValid = RSA.verify(m, receivedSign, rsaPublicKey);
        assertTrue(isValid);
    }

    public void testVerifyWrongSignFail() throws Exception
    {
        String m = "sign message";
        byte[] sign = RSA.sign(m, rsaPrivateKey);
        String base64Sign = Base64.encodeToString(sign, Base64.DEFAULT);
        byte[] receivedSign = Base64.decode(base64Sign, Base64.DEFAULT);
        int hackBit = receivedSign[0];
        hackBit = ~hackBit;
        receivedSign[0] = Byte.parseByte(String.valueOf(hackBit));
        boolean isValid = RSA.verify(m, receivedSign, rsaPublicKey);
        assertFalse(isValid);
    }

    public void testVerifyWrongMessageFail() throws Exception
    {
        String m1 = "sign message";
        String m2 = "sign message2";
        byte[] sign = RSA.sign(m1, rsaPrivateKey);
        String base64Sign = Base64.encodeToString(sign, Base64.DEFAULT);
        byte[] receivedSign = Base64.decode(base64Sign, Base64.DEFAULT);
        boolean isValid = RSA.verify(m2, receivedSign, rsaPublicKey);
        assertFalse(isValid);
    }
}