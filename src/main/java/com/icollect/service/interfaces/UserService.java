package com.icollect.service.interfaces;

import com.icollect.entities.User;

public interface UserService
{
    public int insert(User user);

    public User find(Integer id);
}
