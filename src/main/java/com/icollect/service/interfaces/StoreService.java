package com.icollect.service.interfaces;

import com.icollect.entities.Store;

public interface StoreService
{
    public boolean register(String storeName, String email, String password, String e, String n);

    public Store getStore(Integer sid);

    public boolean login(String email, String password);
}
