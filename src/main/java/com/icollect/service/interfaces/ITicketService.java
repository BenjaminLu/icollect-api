package com.icollect.service.interfaces;

import com.icollect.entities.Ticket;

import java.util.ArrayList;

public interface ITicketService
{
    Ticket sign(int id);
    boolean generateKeyPair();
    ArrayList<Ticket> signUserTickets(int userId);
}
