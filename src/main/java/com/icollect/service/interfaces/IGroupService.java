package com.icollect.service.interfaces;

import com.icollect.entities.Group;

import java.util.ArrayList;

public interface IGroupService
{
    ArrayList<Group> listAll();
}
