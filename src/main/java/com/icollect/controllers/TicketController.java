package com.icollect.controllers;

import com.icollect.entities.Ticket;
import com.icollect.helpers.RSA;
import com.icollect.service.interfaces.ITicketService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.List;

@Controller
public class TicketController
{
    @Autowired
    private ITicketService ticketService;

    @RequestMapping(value = "/generate/keypair", produces = {"application/json; charset=UTF-8"}, method = {RequestMethod.GET})
    @ResponseBody
    public String genKey()
    {
        boolean isSuccessful = ticketService.generateKeyPair();
        if (isSuccessful) {
            JSONObject root = new JSONObject();
            root.put("status", true);
            return root.toString();
        }
        return new JSONObject().toString();
    }

    @RequestMapping(value = "/sign/ticket/{id}", produces = {"application/json; charset=UTF-8"}, method = {RequestMethod.GET})
    @ResponseBody
    public String show(@PathVariable int id)
    {
        Ticket ticket = ticketService.sign(id);
        if (ticket != null && ticket.getSign() != null) {
            JSONObject root = new JSONObject();
            root.put("data", ticket.getData());
            root.put("sign", ticket.getSign());
            return root.toString();
        }
        return new JSONObject().toString();
    }

    @RequestMapping(value = "/sign/user/{uid}/ticket", produces = {"application/json; charset=UTF-8"}, method = {RequestMethod.GET})
    @ResponseBody
    public String signUserTickets(@PathVariable int uid)
    {
        List<Ticket> tickets = ticketService.signUserTickets(uid);
        JSONArray response = new JSONArray();
        if (tickets.size() > 0) {
            for (Ticket ticket : tickets) {
                JSONObject o = new JSONObject();
                o.put("data", ticket.getData());
                o.put("sign", ticket.getSign());
                response.put(o);
            }
            return response.toString();
        }

        return new JSONArray().toString();
    }

    @RequestMapping(value = "/verify", produces = {"application/json; charset=UTF-8"}, method = {RequestMethod.POST})
    @ResponseBody
    public String verify(
            @RequestParam(value = "message", required = true) String message,
            @RequestParam(value = "sign", required = true) String signBase64,
            @RequestParam(value = "modulus", required = true) String modulus,
            @RequestParam(value = "public_exponent", required = true) String publicExponent)
    {
        System.out.println("Client message : " + message);
        System.out.println("Sign Base64 : " + signBase64);
        System.out.println(publicExponent);

        byte[] sign = Base64.decode(signBase64);
        System.out.println("Sign : " + new String(sign));

        RSAPublicKey publicKey = RSA.getPublicKey(new BigInteger(modulus),
                new BigInteger(publicExponent));
        boolean verify = RSA.verify(message, sign, publicKey);

        String m = "From Server";
        KeyPair keyPair = RSA.generateKeyPair();
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyPair.getPrivate();
        RSAPublicKey rsaPublicKey = (RSAPublicKey) keyPair.getPublic();

        byte[] encrypted = RSA.encrypt(m, rsaPublicKey);
        byte[] decrypted = RSA.decrypt(encrypted, rsaPrivateKey);

        System.out.println("Original String : " + new String(decrypted));

        BigInteger serverModulus = rsaPublicKey.getModulus();
        BigInteger serverPublicExponent = rsaPublicKey.getPublicExponent();

        byte[] signFromServer = RSA.sign(m, rsaPrivateKey);
        String signFromServerBase64 = Base64.encode(signFromServer);

        System.out.println("Verify : " + verify);

        JSONObject object = new JSONObject();
        object.put("verify", verify);
        object.put("message", m);
        object.put("sign", signFromServerBase64);
        object.put("modulus", serverModulus.toString());
        object.put("public_exponent", serverPublicExponent.toString());

        return object.toString();
    }
}
