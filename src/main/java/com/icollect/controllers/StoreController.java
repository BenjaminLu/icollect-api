package com.icollect.controllers;

import com.icollect.entities.Store;
import com.icollect.service.interfaces.StoreService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

public class StoreController
{
    @Autowired
    private StoreService storeService;

    @RequestMapping(value = "/store/add", produces = {"application/json; charset=UTF-8"}, method = {RequestMethod.POST})
    @ResponseBody
    public String add(
            @RequestParam(value = "storename", required = true) String storeName,
            @RequestParam(value = "password", required = true) String password,
            @RequestParam(value = "password_check", required = true) String password_check,
            @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "e", required = true) String e,
            @RequestParam(value = "n", required = true) String n
    )
    {
        JSONObject object = new JSONObject();

        if (password.equals(password_check)) {
            boolean status = storeService.register(storeName, email, password, e, n);
            object.put("status", status);
            if (!status) {
                object.put("reason", "somethine went wrong.");
            }
        } else {
            object.put("status", false);
            object.put("reason", "password did not match.");
        }

        return object.toString();
    }

    @RequestMapping(value = "/store/{sid}", produces = {"application/json; charset=UTF-8"}, method = {RequestMethod.GET})
    @ResponseBody
    public String show(@PathVariable int sid)
    {
        JSONObject object = new JSONObject();
        Store store = storeService.getStore(new Integer(sid));
        object.put("sid", store.getSid());
        object.put("storename", store.getStoreName());
        object.put("password", store.getPassword());
        object.put("email", store.getEmail());
        object.put("e", store.getE());
        object.put("n", store.getN());

        return object.toString();
    }

    @RequestMapping(value = "/store/login", produces = {"application/json; charset=UTF-8"}, method = {RequestMethod.POST})
    @ResponseBody
    public String login(
            @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "password", required = true) String password)
    {
        JSONObject object = new JSONObject();
        boolean success = storeService.login(email, password);
        object.put("status", success);

        return object.toString();
    }
}
