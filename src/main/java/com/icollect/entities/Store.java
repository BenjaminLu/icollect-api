package com.icollect.entities;

public class Store
{
    private Integer sid;

    public Integer getSid()
    {
        return sid;
    }

    public void setSid(Integer sid)
    {
        this.sid = sid;
    }

    public String getStoreName()
    {
        return storeName;
    }

    public void setStoreName(String storeName)
    {
        this.storeName = storeName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String passoword)
    {
        this.password = passoword;
    }

    public String getN()
    {
        return n;
    }

    public void setN(String n)
    {
        this.n = n;
    }

    public String getE()
    {
        return e;
    }

    public void setE(String e)
    {
        this.e = e;
    }

    private String storeName;
    private String email;
    private String password;
    private String n;
    private String e;

}
