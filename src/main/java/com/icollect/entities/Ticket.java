package com.icollect.entities;

import java.sql.Timestamp;

public class Ticket
{
    int id;
    String name;
    String body;
    int price;
    String image;
    int activityID;
    int userID;
    boolean status;
    Timestamp createAt;
    Timestamp updateAt;
    String data;
    String sign;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public int getActivityID()
    {
        return activityID;
    }

    public void setActivityID(int activityID)
    {
        this.activityID = activityID;
    }

    public int getUserID()
    {
        return userID;
    }

    public void setUserID(int userID)
    {
        this.userID = userID;
    }

    public boolean getStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public Timestamp getCreateAt()
    {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt)
    {
        this.createAt = createAt;
    }

    public Timestamp getUpdateAt()
    {
        return updateAt;
    }

    public void setUpdateAt(Timestamp updateAt)
    {
        this.updateAt = updateAt;
    }

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public String getSign()
    {
        return sign;
    }

    public void setSign(String sign)
    {
        this.sign = sign;
    }

}
