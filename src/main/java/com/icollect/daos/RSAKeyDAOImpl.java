package com.icollect.daos;

import com.icollect.dao.interfaces.IRSAKeyDAO;
import com.icollect.helpers.Base64;
import com.icollect.helpers.RSA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

@Repository()
public class RSAKeyDAOImpl implements IRSAKeyDAO
{
    @Autowired
    private DataSource dataSource;

    public boolean insertKeyPair(RSAPublicKey publicKey, RSAPrivateKey privateKey)
    {
        Random rnd = new Random();
        int pair = 100000 + rnd.nextInt(900000);
        boolean isPublicSuccess = insert(pair, "public", publicKey.getModulus(), publicKey.getPublicExponent());
        boolean isPrivateSuccess = insert(pair, "private", privateKey.getModulus(), privateKey.getPrivateExponent());

        if (isPublicSuccess && isPrivateSuccess) {
            return true;
        }

        return false;
    }

    private boolean insert(int pair, String type, BigInteger modulus, BigInteger exponent)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String base64Modulus = Base64.encodeToString(modulus.toByteArray(), Base64.DEFAULT);
        String base64Exponent = Base64.encodeToString(exponent.toByteArray(), Base64.DEFAULT);
        String sql = "INSERT INTO rsa_keys (pair, type, modulus, exponent) VALUES (?, ?, ?, ?)";
        int insertID = 0;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, pair);
            ps.setString(2, type);
            ps.setString(3, base64Modulus);
            ps.setString(4, base64Exponent);
            insertID = ps.executeUpdate();

            if (insertID != 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public RSAPrivateKey getLastPrivateKey()
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM rsa_keys WHERE type = ? ORDER BY id DESC";
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "private");
            rs = ps.executeQuery();

            if (rs.next()) {
                String base64Modulus = rs.getString("modulus");
                String base64Exponent = rs.getString("exponent");
                BigInteger modulus = new BigInteger(Base64.decode(base64Modulus, Base64.DEFAULT));
                BigInteger privateExponent = new BigInteger(Base64.decode(base64Exponent, Base64.DEFAULT));
                RSAPrivateKey privateKey = RSA.getPrivateKey(modulus, privateExponent);
                return privateKey;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public RSAPublicKey getLastPublicKey()
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM rsa_keys WHERE type = ? ORDER BY id DESC";
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "public");
            rs = ps.executeQuery();

            if (rs.next()) {
                String base64Modulus = rs.getString("modulus");
                String base64Exponent = rs.getString("exponent");
                BigInteger modulus = new BigInteger(Base64.decode(base64Modulus, Base64.DEFAULT));
                BigInteger publicExponent = new BigInteger(Base64.decode(base64Exponent, Base64.DEFAULT));
                RSAPublicKey publicKey = RSA.getPublicKey(modulus, publicExponent);
                return publicKey;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
