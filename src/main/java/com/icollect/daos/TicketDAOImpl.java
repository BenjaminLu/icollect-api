package com.icollect.daos;

import com.icollect.dao.interfaces.ITicketDAO;
import com.icollect.entities.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;

@Repository()
public class TicketDAOImpl implements ITicketDAO
{
    @Autowired
    private DataSource dataSource;

    @Override
    public Ticket find(int id)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM tickets WHERE id = ?";
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                int tid = rs.getInt("id");
                String name = rs.getString("name");
                String body = rs.getString("body");
                String image = rs.getString("image");
                int activityID = rs.getInt("activity_id");
                int userID = rs.getInt("user_id");
                boolean status = rs.getBoolean("status");
                Timestamp createdAt = rs.getTimestamp("created_at");
                Timestamp updatedAt = rs.getTimestamp("updated_at");
                Ticket ticket = new Ticket();
                ticket.setId(tid);
                ticket.setName(name);
                ticket.setBody(body);
                ticket.setImage(image);
                ticket.setActivityID(activityID);
                ticket.setUserID(userID);
                ticket.setStatus(status);
                ticket.setCreateAt(createdAt);
                ticket.setUpdateAt(updatedAt);

                return ticket;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public boolean updateSign(int id, String base64Sign)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "UPDATE tickets SET sign = ? WHERE id = ?";
        int affectedRows = 0;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, base64Sign);
            ps.setInt(2, id);
            affectedRows = ps.executeUpdate();

            if (affectedRows != 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public ArrayList<Ticket> findUserTickets(int uid)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM tickets WHERE user_id = ?";
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, uid);
            rs = ps.executeQuery();
            ArrayList<Ticket> tickets = new ArrayList<>();
            while (rs.next()) {
                int tid = rs.getInt("id");
                String name = rs.getString("name");
                String body = rs.getString("body");
                String image = rs.getString("image");
                int activityID = rs.getInt("activity_id");
                int userID = rs.getInt("user_id");
                String sign = rs.getString("sign");
                boolean status = rs.getBoolean("status");
                Timestamp createdAt = rs.getTimestamp("created_at");
                Timestamp updatedAt = rs.getTimestamp("updated_at");
                Ticket ticket = new Ticket();
                ticket.setId(tid);
                ticket.setName(name);
                ticket.setBody(body);
                ticket.setImage(image);
                ticket.setActivityID(activityID);
                ticket.setUserID(userID);
                ticket.setSign(sign);
                ticket.setStatus(status);
                ticket.setCreateAt(createdAt);
                ticket.setUpdateAt(updatedAt);
                tickets.add(ticket);
            }
            return tickets;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
