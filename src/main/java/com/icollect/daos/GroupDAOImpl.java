package com.icollect.daos;

import com.icollect.dao.interfaces.IGroupDAO;
import com.icollect.entities.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Repository()
public class GroupDAOImpl implements IGroupDAO
{

    @Autowired
    private DataSource dataSource;

    @Override
    public ArrayList<Group> all()
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM groups";
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            ArrayList<Group> groups = new ArrayList<Group>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");

                Group group = new Group();
                group.setId(id);
                group.setName(name);
                groups.add(group);
            }

            return groups;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
