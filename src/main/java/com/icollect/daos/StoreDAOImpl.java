package com.icollect.daos;

import com.icollect.dao.interfaces.StoreDAO;
import com.icollect.entities.Store;
import com.icollect.setting.PasswordSalt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository()
public class StoreDAOImpl implements StoreDAO
{

    @Autowired
    private DataSource dataSource;

    @Override
    public int insert(Store store)
    {
        String storeName = store.getStoreName();
        String email = store.getEmail();
        String password = store.getPassword();
        String e = store.getE();
        String n = store.getN();

        MessageDigestPasswordEncoder encoder = new MessageDigestPasswordEncoder("SHA-1");
        String hashed_password = encoder.encodePassword(password, PasswordSalt.storeSalt);

        Connection conn = null;
        PreparedStatement ps = null;
        int line = 0;
        try {
            conn = dataSource.getConnection();
            String sql = "INSERT INTO stores(store_name, email, password, e, n) VALUES(?, ?, ?, ?, ?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1, storeName);
            ps.setString(2, email);
            ps.setString(3, hashed_password);
            ps.setString(4, e);
            ps.setString(5, n);

            line = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return line;
    }

    @Override
    public Store find(Integer sid)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM stores WHERE sid = ?";
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, sid.intValue());
            rs = ps.executeQuery();

            if (rs.next()) {
                Integer sidNumber = new Integer(rs.getInt(1));
                String storeName = rs.getString(2);
                String email = rs.getString(3);
                String password = rs.getString(4);
                String e = rs.getString(5);
                String n = rs.getString(6);

                Store store = new Store();
                store.setSid(sidNumber);
                store.setStoreName(storeName);
                store.setEmail(email);
                store.setPassword(password);
                store.setE(e);
                store.setN(n);

                return store;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public Store finbByEmail(String email)
    {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM stores WHERE email = ?";
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();

            if (rs.next()) {
                Integer sid = new Integer(rs.getInt(1));
                String storeName = rs.getString(2);
                String storeEmail = rs.getString(3);
                String password = rs.getString(4);
                String e = rs.getString(5);
                String n = rs.getString(6);

                Store store = new Store();
                store.setSid(sid);
                store.setStoreName(storeName);
                store.setEmail(storeEmail);
                store.setPassword(password);
                store.setE(e);
                store.setN(n);

                return store;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
