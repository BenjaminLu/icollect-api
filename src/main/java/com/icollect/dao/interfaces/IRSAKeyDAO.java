package com.icollect.dao.interfaces;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public interface IRSAKeyDAO
{
    boolean insertKeyPair(RSAPublicKey publicKey, RSAPrivateKey privateKey);

    RSAPrivateKey getLastPrivateKey();

    RSAPublicKey getLastPublicKey();
}
