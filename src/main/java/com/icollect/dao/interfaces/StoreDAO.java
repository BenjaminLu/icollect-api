package com.icollect.dao.interfaces;

import com.icollect.entities.Store;

public interface StoreDAO
{
    public int insert(Store store);

    public Store find(Integer sid);

    public Store finbByEmail(String email);
}
