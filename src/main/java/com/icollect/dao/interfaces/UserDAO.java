package com.icollect.dao.interfaces;

import com.icollect.entities.User;

public interface UserDAO
{
    public int insert(User user);

    public User find(Integer id);
}
