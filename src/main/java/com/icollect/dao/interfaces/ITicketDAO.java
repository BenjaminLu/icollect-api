package com.icollect.dao.interfaces;

import com.icollect.entities.Ticket;

import java.util.ArrayList;

public interface ITicketDAO
{
    Ticket find(int id);

    boolean updateSign(int id, String base64Sign);

    ArrayList<Ticket> findUserTickets(int uid);
}
