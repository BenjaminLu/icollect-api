package com.icollect.dao.interfaces;

import com.icollect.entities.Group;

import java.util.ArrayList;

public interface IGroupDAO
{
    ArrayList<Group> all();
}
