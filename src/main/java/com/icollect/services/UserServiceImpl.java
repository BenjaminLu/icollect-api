package com.icollect.services;

import com.icollect.dao.interfaces.UserDAO;
import com.icollect.entities.User;
import com.icollect.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService
{
    @Autowired
    private UserDAO userDAO;

    @Override
    public int insert(User user)
    {
        int line = userDAO.insert(user);
        return line;
    }

    @Override
    public User find(Integer id)
    {
        User user = userDAO.find(id);
        return user;
    }

}
