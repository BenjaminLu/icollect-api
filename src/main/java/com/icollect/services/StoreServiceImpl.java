package com.icollect.services;

import com.icollect.dao.interfaces.StoreDAO;
import com.icollect.entities.Store;
import com.icollect.service.interfaces.StoreService;
import com.icollect.setting.PasswordSalt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class StoreServiceImpl implements StoreService
{

    @Autowired
    private StoreDAO storeDAO;

    @Override
    public boolean register(String storeName, String email, String password, String e, String n)
    {
        Store store = new Store();
        store.setStoreName(storeName);
        store.setEmail(email);
        store.setPassword(password);
        store.setE(e);
        store.setN(n);

        int line = storeDAO.insert(store);

        if (line > 0) {
            return true;
        }

        return false;
    }

    @Override
    public Store getStore(Integer sid)
    {
        Store store = storeDAO.find(sid);

        if (store == null) {
            throw new NullPointerException();
        }

        return store;
    }

    @Override
    public boolean login(String email, String password)
    {
        Store store = storeDAO.finbByEmail(email);
        String dbPasswordHashed = store.getPassword();

        MessageDigestPasswordEncoder encoder = new MessageDigestPasswordEncoder("SHA-1");
        String hashed_password = encoder.encodePassword(password, PasswordSalt.storeSalt);

        if (hashed_password.equals(dbPasswordHashed)) {
            return true;
        }

        return false;
    }

}
