package com.icollect.services;

import com.icollect.dao.interfaces.IGroupDAO;
import com.icollect.entities.Group;
import com.icollect.service.interfaces.IGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class GroupServiceImpl implements IGroupService
{
    @Autowired
    private IGroupDAO groupDAO;

    @Override
    public ArrayList<Group> listAll()
    {
        return groupDAO.all();
    }


}
