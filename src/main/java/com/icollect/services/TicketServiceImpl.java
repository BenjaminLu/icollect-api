package com.icollect.services;

import com.icollect.dao.interfaces.IRSAKeyDAO;
import com.icollect.dao.interfaces.ITicketDAO;
import com.icollect.entities.Ticket;
import com.icollect.helpers.Base64;
import com.icollect.helpers.RSA;
import com.icollect.service.interfaces.ITicketService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;

@Service
public class TicketServiceImpl implements ITicketService
{

    @Autowired
    private ITicketDAO ticketDAO;
    @Autowired
    private IRSAKeyDAO rsaKeyDAO;
    private volatile RSAPrivateKey privateKey = null;
    private volatile RSAPublicKey publicKey = null;

    public boolean generateKeyPair()
    {
        boolean isSuccess = false;
        privateKey = rsaKeyDAO.getLastPrivateKey();
        publicKey = rsaKeyDAO.getLastPublicKey();
        if (privateKey == null && publicKey == null) {
            KeyPair keyPair = RSA.generateKeyPair();
            privateKey = (RSAPrivateKey) keyPair.getPrivate();
            publicKey = (RSAPublicKey) keyPair.getPublic();
            isSuccess = rsaKeyDAO.insertKeyPair(publicKey, privateKey);
        } else if(privateKey != null && publicKey != null) {
            isSuccess = true;
        }

        return isSuccess;
    }


    @Override
    public Ticket sign(int id)
    {
        Ticket ticket = ticketDAO.find(id);
        boolean isSuccess = false;
        if (privateKey == null && publicKey == null) {
            System.out.println("try get last Key");
            privateKey = rsaKeyDAO.getLastPrivateKey();
            publicKey = rsaKeyDAO.getLastPublicKey();

            if (privateKey != null && publicKey != null) {
                isSuccess = true;
            } else if (privateKey == null && publicKey == null) {
                System.out.println("generate");
                KeyPair keyPair = RSA.generateKeyPair();
                privateKey = (RSAPrivateKey) keyPair.getPrivate();
                publicKey = (RSAPublicKey) keyPair.getPublic();
                isSuccess = rsaKeyDAO.insertKeyPair(publicKey, privateKey);
            }
        } else {
            isSuccess = true;
        }

        if (isSuccess && ticket != null) {
            JSONObject o = new JSONObject();
            o.put("id", ticket.getId());
            o.put("name", ticket.getName());
            o.put("body", ticket.getBody());
            o.put("image", ticket.getImage());
            o.put("activity_id", ticket.getActivityID());
            o.put("user_id", ticket.getUserID());
            o.put("status", ticket.getStatus());

            String m = o.toString();
            ticket.setData(m);
            byte[] sign = RSA.sign(m, privateKey);
            String base64Sign = Base64.encodeToString(sign, Base64.DEFAULT);
            boolean isUpdated = ticketDAO.updateSign(id, base64Sign);
            if (isUpdated) {
                ticket.setSign(base64Sign);
            }
        }

        return ticket;
    }

    @Override
    public ArrayList<Ticket> signUserTickets(int userId)
    {
        ArrayList<Ticket> tickets = ticketDAO.findUserTickets(userId);
        for (Ticket ticket : tickets) {
            JSONObject o = new JSONObject();
            o.put("id", ticket.getId());
            o.put("name", ticket.getName());
            o.put("body", ticket.getBody());
            o.put("image", ticket.getImage());
            o.put("activity_id", ticket.getActivityID());
            o.put("user_id", ticket.getUserID());
            o.put("status", ticket.getStatus());

            String m = o.toString();
            ticket.setData(m);
        }

        return tickets;
    }
}
